#ifndef __TFLITE_DETECTOR_HPP__
#define __TFLITE_DETECTOR_HPP__

#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>
#include <tensorflow/lite/tools/gen_op_registration.h>
#include <tensorflow/lite/builtin_op_data.h>

#include <memory>
#include <map>
#include <string>

#include <opencv2/core/core.hpp>

namespace exercise
{

    class TFLiteDetector
    {

    protected:
        std::unique_ptr<tflite::Interpreter> _interpreter;
        std::shared_ptr<tflite::FlatBufferModel> _model;
        int _input_width, _input_height;
        int _input_channels;
        cv::Mat _input_mat, _temp_mat;

    public:
        TFLiteDetector(const std::string &_model_path, int number_of_threads = 2);
        std::vector<cv::Rect> detect(const cv::Mat &image);
        virtual ~TFLiteDetector();
    };

}

#endif