# Exercise

## Environment Setup

1 - Install the packages from [Dependencies](#Dependencies) list.

2 - Clone the exercise repository to /path/to/excercese

2 - Build and install flatbuffers lib following [instructions](#Flatbuffers)

### Dependencies

#### System Packages

```bash
$> sudo apt install -y cmake build-essential libflatbuffers-dev libopencv-dev libfarmhash-dev gdb
```

#### Flatbuffers

- Download version 1.12.0 from [here](https://github.com/google/flatbuffers/releases/tag/v1.12.0)
- Extract the package
- Build and install

```bash
$> cd flatbuffers-1.12.0
$> mkdir build
$> cd build
$> cmake -D FLATBUFFERS_BUILD_FLATLIB=ON ..
$> make
$> sudo make install
```

## Build

```bash
$> g++ src/main.cpp src/tfdetector.cpp -g3 --std=c++17 -I./include -I./tflite/include/ -I/usr/include/opencv4 -L./tflite/lib -lflatbuffers -ltensorflow-lite -lpthread -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_imgcodecs -o main
```

## Run

```bash
$> LD_LIBRARY_PATH=./tflite/lib ./main
```

## The exercise

The supplied code repository contains a wrapper for TFLite Object detection functionality - `exercise::TFLiteDetector` class.
The model is loaded from the disk once upon initialization.
Each call of a `detect` method on an opencv image, returns a list of rectangles identifying various objects in the image.

The objective of the exercise is write a code based on the supplied example.

1 - The application should read a set of images from disk

2 - The application should run detection on the images

3 - The application should draw the detections and save the images to the disk.

The images should be processed in order.

**The throughput should be optimal**, meaning that the application should achieve **maximal FPS possible**. Use multi-threading if needed.

Bonus - Use a camera stream instead of images.
