#include "tfdetector.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <filesystem>

int main(int argc, const char *argv[])
{
    std::string image_name = argc > 1 ? argv[1]: "00001.jpg";
    std::filesystem::path input_dir = "./images";
    std::filesystem::path output_dir = "./out";
    std::filesystem::create_directory(output_dir);
    
    exercise::TFLiteDetector detector("./model/model.tflite");

    auto image = cv::imread( input_dir / image_name);
    auto bounding_boxes = detector.detect(image);

    for (auto &&bbox: bounding_boxes) {
        cv::rectangle(image, bbox, cv::Scalar(255, 0, 0));
    }
   
    cv::imwrite(output_dir / image_name, image);

    return 0;
}