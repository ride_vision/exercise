#include "tfdetector.hpp"

#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>
#include <tensorflow/lite/tools/gen_op_registration.h>

#include <opencv2/imgproc.hpp>

#include <iostream>
#include <algorithm>

using std::max;
using std::min;

exercise::TFLiteDetector::TFLiteDetector(const std::string &_model_path, int number_of_threads)
{

    _model = tflite::FlatBufferModel::BuildFromFile(_model_path.c_str());

    if (!_model)
    {
        throw std::runtime_error("Failed to load tflite model: " + _model_path);
    }
    std::cout << "Loaded model " << _model_path << std::endl;

    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder builder(*_model, resolver);
    _interpreter = std::make_unique<tflite::Interpreter>();
    builder(&_interpreter, number_of_threads);

    std::cout << "Created interpreter" << std::endl;

    if (!_interpreter->AllocateTensors() == kTfLiteOk)
    {
        throw "Failed to allocate tensors";
    }

    auto input_tensor = _interpreter->tensor(_interpreter->inputs()[0]);
    auto input_tensor_dims = input_tensor->dims;

    _input_width = input_tensor_dims->data[2];
    _input_height = input_tensor_dims->data[1];
    _input_channels = input_tensor_dims->data[3];

    auto input = _interpreter->typed_input_tensor<float>(0);

    std::cout << "Input tensor " << _input_width << "x" << _input_height << "x" << _input_channels << std::endl;
    _input_mat = cv::Mat(_input_height, _input_width, CV_32FC(this->_input_channels), input, 0);
}
exercise::TFLiteDetector::~TFLiteDetector()
{
}

std::vector<cv::Rect> exercise::TFLiteDetector::detect(const cv::Mat &image)
{
    cv::resize(image, _temp_mat, cv::Size(_input_width, _input_height));
    cv::cvtColor(_temp_mat, _temp_mat, cv::COLOR_BGR2RGB);
    _temp_mat.convertTo(_input_mat, CV_32FC3);

    _input_mat = _input_mat / 128.0 - 1.0;

    assert(_interpreter->Invoke() == kTfLiteOk);

    auto output_classes = _interpreter->tensor(_interpreter->outputs()[1]);
    auto output_locations = _interpreter->tensor(_interpreter->outputs()[0]);
    auto output_scores = _interpreter->tensor(_interpreter->outputs()[2]);
    auto output_number_of_dets = _interpreter->tensor(_interpreter->outputs()[3]);
    auto number_of_dets = output_number_of_dets->data.f[0];

    auto output_data = output_locations->data.f;

    std::vector<cv::Rect> bounding_boxes;

    for (int i = 0; i < number_of_dets; i++)
    {
        auto det_score = output_scores->data.f[i];
        if (det_score <= 0.75)
        {
            continue;
        }
        cv::Point tl(max(0, int(output_data[i * 4 + 1] * image.cols)), max(0, int(output_data[i * 4] * image.rows)));
        cv::Point br(min(int(output_data[i * 4 + 3] * image.cols), image.cols), min(int(output_data[i * 4 + 2] * image.rows), image.rows));
        
        bounding_boxes.push_back({tl, br});
        
    }
    return bounding_boxes;
}